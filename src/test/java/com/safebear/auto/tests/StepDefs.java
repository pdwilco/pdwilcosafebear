package com.safebear.auto.tests;

//import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {

    WebDriver driver;


    LoginPage loginpage;
    ToolsPage toolspage;


    @Before
    public void setUp() {
        driver = Utils.getDriver();

        loginpage = new LoginPage(driver);
        toolspage = new ToolsPage(driver);

    }


    @After
    public void tearDown() {
        //this pauses the test for 2 seconds

        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //This quites the browser after each scenario
        driver.quit();
    }


    @Given("I navigate to the login page")
    public void i_navigate_to_the_login_page() {
        //Step 1 ACTION: Open our Web Application in the browser
        driver.get(Utils.getUrl());
        Assert.assertEquals(loginpage.getPageTitle(), "Login Page", "We're not on the Login Page or it's title has changed");
    }

    @When("I enter the login details for a {string}")
    public void i_enter_the_login_details_for_a(String userType) {
        switch (userType) {
            case "invalidUser":
                loginpage.enterUsername("attacker");
                loginpage.enterPassword("dontletmein");
                loginpage.clickLoginButton();
                break;

            case "validUser":
                loginpage.enterUsername("tester");
                loginpage.enterPassword("letmein");
                loginpage.clickLoginButton();
                break;
            default:
                Assert.fail("The test data is wrong. The only values that can be accepted are 'validUser' or 'invalidUser");
                break;
        }
    }


    @Then("I can see the following message: {string}")
    public void i_can_see_the_following_message(String validationMessage) {
        switch (validationMessage) {
            case "Username or Password is incorrect":
                Assert.assertTrue(loginpage.checkForFailedLoginWarning().contains(validationMessage));
                break;
            case "Login Successful":
                Assert.assertTrue(toolspage.checkForLoginSuccessfulMessage().contains(validationMessage));
                break;
            default:
                Assert.fail("The test data is wrong");
                break;
        }
    }
}