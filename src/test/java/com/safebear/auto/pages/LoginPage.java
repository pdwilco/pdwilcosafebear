package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();

    @NonNull
    WebDriver driver;


    //Get Page Title

    public String getPageTitle(){
        return driver.getTitle(); }

    //Get User Name
    public void enterUsername(String username){

        driver.findElement(locators.getUsernameLocator()).sendKeys(username);


    }


    //Get Password
    public void enterPassword(String password) {

        driver.findElement(locators.getPasswordLocator()).sendKeys(password);


    }
    //click Login button
    public void clickLoginButton(){

        driver.findElement(locators.getLoginButtonLocator()).click();
    }

    //Check Failed Login

    public String checkForFailedLoginWarning(){

        return driver.findElement(locators.getFailedLoginMessage()).getText();
    }

}
