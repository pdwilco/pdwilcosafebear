package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;


@Data

public class LoginPageLocators {

    //
    //inputs
    //
    private By usernameLocator = By.id("username");

    //buttons

    private By passwordLocator = By.xpath(".//input[@id=\"password\"]");

    private By loginButtonLocator = By.xpath(".//*[@id='enter']");

    //messages

    private By failedLoginMessage = By.xpath("//p[@id=\"rejectLogin\"]");





}
